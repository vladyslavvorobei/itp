How to start project:

On your local machine have to be installed the latest version of node.js and npm
(or you could use yarn if you want).
Download link: `https://nodejs.org/en/`

Checking in cmd: 
1. `node --version` (min v9.2.1)
2. `npm --version` (min v5.5.1)

Building bundle: 
1. Install packages `npm install`.
2. Run `npm run start` to start the local server on port 3000,
also, you could use your local ip for checking the website on mobile devices or another local machine.
3. Run `npm run clean` - delete all files and folders in the dist directory.
4. Run `npm run prod` - production, all css and js files are minified and compressed.

Path's: 

1. HTML content in handlebars (tamplate builder) `./src/hbs` files, all files imported to `./dist/`.
2. SCSS in `src/scss` folder, all files imported `./dist/css/styles.css`.
3. JS main file in `./src/app.js` + files from `./src/js` folder, all files imported `./dist/css/app.js`.
4. All assets (like images, fonts, json files) just copied to dist folder using CopyWebpackPlugin in the webpack,config.js file.


