import slick from "slick-carousel";

//var
const popUpContainer = document.getElementsByClassName('popup'); 
const mainBody = document.getElementsByTagName('body');
const firstPopUp = document.getElementsByClassName('popup__first');
const secondPopUp = document.getElementsByClassName('popup__second');
const thirdPopUp = document.getElementsByClassName('popup__third');
const fourthPopUp = document.getElementsByClassName('popup__fourth');
const navigation = document.querySelector('header');

//Get current year
const currentYear = () => {
    let container = document.getElementById('currentYear');
    let date = new Date();
    let year = date.getFullYear();
    container.innerHTML = year;
}
//init functions
currentYear();

const ua = navigator.userAgent,
    iOS = /iPad|iPhone|iPod/.test(ua),
    iOS11 = /OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2|OS 11_2_1/.test(ua);

const bugIos11 = () => {
    if ( iOS && iOS11 ) {
        document.body.classList.add('iosBug');
    }
}

const niceHeader = () => {
    if ( iOS && iOS11 ) {
        navigation.style = 'background-color: rgba(255,255,255,0.8);-webkit-backdrop-filter: blur(5px);';
    }
}
niceHeader();

//Open popup
window.openPopup = (item) => {
    popUpContainer[0].style.cssText = 'opacity: 1; z-index: 200';
    bugIos11();
    if(item === 1) {
        firstPopUp[0].style.display = 'block';
    }
    else if(item === 2) {
        secondPopUp[0].style.display = 'block';
    }
    else if(item === 3) {
        thirdPopUp[0].style.display = 'block';
    } else {
        fourthPopUp[0].style.display = 'block';
    }
}

//Close popup
window.closePopup = () => {
    document.body.classList.remove('iosBug');
    popUpContainer[0].style.cssText = 'opacity: 0; z-index: -1';
    firstPopUp[0].style.display = 'none';
    secondPopUp[0].style.display = 'none';
    thirdPopUp[0].style.display = 'none';
    fourthPopUp[0].style.display = 'none';
}


window.onload = () => {
    $('.modulesSlider').slick({
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: "<img src='./assets/img/arrow-left.png' alt='left arrow' class='homeSlider__btn homeSlider__btn_left'/>",
        nextArrow: "<img src='./assets/img/arrow-right.png' alt='right arrow' class='homeSlider__btn homeSlider__btn_right'/>",
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false
                }
            }
        ]
    });

    setTimeout("closePopup(); openPopup(4);", 60000);
}

const energy = document.querySelectorAll('.energy-container');
const elements = document.querySelectorAll('.circle-move');

function AddHeight(height) {
    for (let index = 0; index < elements.length; index++) {
        elements[index].style.top = `${height}px`
    }
}

for (let i = 0; i < energy.length; i++) {
    energy[i].addEventListener('mouseenter', () => {
        if(i === 0) {
            AddHeight(0);
        }
        else if(i === 1) {
            AddHeight(240);
        }
        else if(i === 2) {
            AddHeight(440);
        }
    })
}


// add accordion

const items = document.querySelectorAll(".accordion a");

function toggleAccordion(){
    this.classList.toggle('active');
    this.nextElementSibling.classList.toggle('active');
    this.nextElementSibling.slideToggle('active');
}

items.forEach(item => item.addEventListener('click', toggleAccordion));

