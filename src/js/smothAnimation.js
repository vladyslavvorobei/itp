import SmoothScroll from 'smooth-scroll';


var 		scroll = new SmoothScroll();
const 	nav = document.querySelector('.nav');
const		humburger = document.querySelector('.humburger');


var smoothScrollWithoutHash = function (selector, settings) {
	/**
	 * If smooth scroll element clicked, animate scroll
	 */
	var clickHandler = function (event) {
		var toggle = event.target.closest( selector );
		//console.log(toggle);
		if ( !toggle || toggle.tagName.toLowerCase() !== 'a' ) return;
		//console.log(toggle.hash);
		var anchor = document.querySelector( toggle.hash );
		if ( !anchor ) return;

		event.preventDefault(); // Prevent default click event
		// mobile behaviour of navigation bar
		if(window.innerHeight < 1025) {
			setTimeout(() => {
				nav.classList.remove('nav--visible');
				humburger.classList.remove('humburger--active');
			}, 250);
		}
		scroll.animateScroll( anchor, toggle, settings || {} ); // Animate scroll
	};

	window.addEventListener('click', clickHandler, false );
};

// Run our function
smoothScrollWithoutHash( 'a[href*="#"]',{
	header: '[data-scroll-header]',
	offset: '83px',
} );