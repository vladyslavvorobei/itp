window.spamPopupForm = (e) => {
    e.preventDefault();
    //console.log('form fiered');
    var answer = document.getElementById('requestSpamPopup');
    var form = document.getElementById('formSpamPopup');
    var namePure = document.getElementById("nameSpamPopup").value;
    var name = encodeURIComponent(document.getElementById("nameSpamPopup").value);
    var tel = encodeURIComponent(document.getElementById("telSpamPopup").value);
    var email = encodeURIComponent(document.getElementById("emailSpamPopup").value);
    //console.log(`${name}, ${tel}, ${email}`);
    var params = "name=" + name + "&tel=" + tel + "&email=" + email;

    var request = new XMLHttpRequest();
    request.open("POST","php/code/spam.php",true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = () => {
        form.classList.add('form__form--hide');
        if (request.status >= 200 && request.status < 400) {
            console.log('Success!');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> Мы свяжемся с вами в ближайшее время.
                </div>
            `;
            answer.classList.add('popup--active');

        } else {
            // console.log('We reached our target server, but it returned an error');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> К сожалению данные не отправлены, ошибка сервера. 
                </div>
            `;
            answer.classList.add('popup--active');
            console.error('We reached our target server, but it returned an error');
        }
    };

    request.onerror = () => {
        console.error('There was a connection error of some sort');
    };

    request.send(params);
}

window.orderPopupForm = (e) => {
    e.preventDefault();
    //console.log('form fiered');
    var answer = document.getElementById('requestOrderPopup');
    var form = document.getElementById('formOrderPopup');
    var namePure = document.getElementById("nameOrderPopup").value;
    var name = encodeURIComponent(document.getElementById("nameOrderPopup").value);
    var tel = encodeURIComponent(document.getElementById("telOrderPopup").value);
    var email = encodeURIComponent(document.getElementById("emailOrderPopup").value);
    //console.log(`${name}, ${tel}, ${email}`);
    var params = "name=" + name + "&tel=" + tel + "&email=" + email;

    var request = new XMLHttpRequest();
    request.open("POST","php/code/order.php",true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = () => {
        form.classList.add('form__form--hide');
        if (request.status >= 200 && request.status < 400) {
            console.log('Success!');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> Мы свяжемся с вами в ближайшее время.
                </div>
            `;
            answer.classList.add('popup--active');

        } else {
            // console.log('We reached our target server, but it returned an error');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> К сожалению данные не отправлены, ошибка сервера. 
                </div>
            `;
            answer.classList.add('popup--active');
            console.error('We reached our target server, but it returned an error');
        }
    };

    request.onerror = () => {
        console.error('There was a connection error of some sort');
    };

    request.send(params);
}


window.orderForm = (e) => {
    e.preventDefault();
    //console.log('form fiered');
    var answer = document.getElementById('requestOrder');
    var form = document.getElementById('formOrder');
    var namePure = document.getElementById("nameOrder").value;
    var name = encodeURIComponent(document.getElementById("nameOrder").value);
	var tel = encodeURIComponent(document.getElementById("telOrder").value);
    var email = encodeURIComponent(document.getElementById("emailOrder").value);
    //console.log(`${name}, ${tel}, ${email}`);
    var params = "name=" + name + "&tel=" + tel + "&email=" + email;
    
    var request = new XMLHttpRequest(); 
    request.open("POST","php/code/order.php",true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = () => {
        form.classList.add('form__form--hide');
        if (request.status >= 200 && request.status < 400) {
            console.log('Success!');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> Мы свяжемся с вами в ближайшее время.
                </div>
            `;
            answer.classList.add('requestOrder--active');
            
        } else {
            // console.log('We reached our target server, but it returned an error');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> К сожалению данные не отправлены, ошибка сервера. 
                </div>
            `;
            answer.classList.add('requestOrder--active');
            console.error('We reached our target server, but it returned an error');
        }
    };

    request.onerror = () => {
        console.error('There was a connection error of some sort');
    };

    request.send(params);
}



window.consultationForm = (e) => {
    e.preventDefault();
    var answer = document.getElementById('consultationRequest');
    var form = document.getElementById('consultationForm');
    var namePure = document.getElementById("name1").value;
    var name = encodeURIComponent(document.getElementById("name1").value);
	var tel = encodeURIComponent(document.getElementById("tel1").value);
    
    var params = "name=" + name + "&tel=" + tel;

    var request = new XMLHttpRequest(); 
    request.open("POST","php/code/consultation.php",true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = () => {
        form.classList.add('form__form--hide');
        if (request.status >= 200 && request.status < 400) {
            console.log('Success!');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> Мы свяжемся с вами в ближайшее время.
                </div>
            `;
            answer.classList.add('popup--active');
            
        } else {
            //console.log('We reached our target server, but it returned an error');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> К сожалению данные не отправлены, ошибка сервера. 
                </div>
            `;
            answer.classList.add('popup--active');
            console.error('We reached our target server, but it returned an error');
        }
    };

    request.onerror = () => {
        console.error('There was a connection error of some sort');
    };

    request.send(params);
}


window.phoneForm = (e) => {
    e.preventDefault();
    var answer = document.getElementById('phoneRequest');
    var form = document.getElementById('phoneForm');
    var namePure = document.getElementById("name2").value;
    var name = encodeURIComponent(document.getElementById("name2").value);
	var tel = encodeURIComponent(document.getElementById("tel2").value);
    
    var params = "name=" + name + "&tel=" + tel;

    var request = new XMLHttpRequest(); 
    request.open("POST","php/code/phone.php",true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = () => {
        form.classList.add('form__form--hide');
        if (request.status >= 200 && request.status < 400) {
            console.log('Success!');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> Мы свяжемся с вами в ближайшее время.
                </div>
            `;
            answer.classList.add('popup--active');
            
        } else {
            //console.log('We reached our target server, but it returned an error');
            answer.innerHTML = `
                <div>
                    Спасибо, <b>${namePure}</b>!<br> К сожалению данные не отправлены, ошибка сервера. 
                </div>
            `;
            answer.classList.add('popup--active');
            console.error('We reached our target server, but it returned an error');
        }
    };

    request.onerror = () => {
        console.error('There was a connection error of some sort');
    };

    request.send(params);
}
