//styles
import css from './scss/styles.scss';

import './js/smothAnimation.js';
import './js/main.js';
import './js/form.js';
import './js/mobile.js';
import './js/mobileHover.js';