<?php

require '../vendor/autoload.php';

use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;

putenv('GOOGLE_APPLICATION_CREDENTIALS=../client_secret.json');
$client = new Google_Client;
$client->useApplicationDefaultCredentials();

$client->setApplicationName("Something to do with my representatives");
$client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);

if ($client->isAccessTokenExpired()) {
    $client->refreshTokenWithAssertion();
}

$accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
ServiceRequestFactory::setInstance(
    new DefaultServiceRequest($accessToken)
);

// Get our spreadsheet
$spreadsheet = (new Google\Spreadsheet\SpreadsheetService)
    ->getSpreadsheetFeed()
    ->getByTitle('ITP');

// Get the first worksheet (tab)
$worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
$worksheet = $worksheets[0];

$listFeed = $worksheet->getListFeed();

$name = $_POST['name'];
$email = $_POST['email'];
$tel = $_POST['tel'];
$name =  htmlspecialchars($name, ENT_QUOTES);
$email = htmlspecialchars($email, ENT_QUOTES);
$tel = htmlspecialchars($tel, ENT_QUOTES);
$date = date('d.m.Y');

$listFeed->insert([
    'name' => $name,
    'email' => $email,
    'phone' => $tel,
    'date' => $date,
]);

$hire_us_message = 'Имя: ' . $name . "\r\n";
$hire_us_message .= 'Телефон: ' . $tel . "\r\n";
$hire_us_message .= 'Email: ' . $email . "\r\n";
$file = fopen('order.csv', 'a');
$request = array($name, $tel, $email);
fputcsv($file, $request);
fclose($file);
$to = 'as.antonuk@gmail.com, info@sempal.com.ua';
$subject = 'Лендинг ИТП: запрос "УТОЧНИТЬ ЦЕНУ"';
$headers = 'From: info@sempal.com.ua' . "\r\n" . 'Reply-To: ' . $email;
mail($to, $subject, $hire_us_message, $headers);
