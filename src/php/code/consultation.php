<?php

require '../vendor/autoload.php';

use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;

putenv('GOOGLE_APPLICATION_CREDENTIALS=../client_secret.json');
$client = new Google_Client;
$client->useApplicationDefaultCredentials();

$client->setApplicationName("Something to do with my representatives");
$client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);

if ($client->isAccessTokenExpired()) {
    $client->refreshTokenWithAssertion();
}

$accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
ServiceRequestFactory::setInstance(
    new DefaultServiceRequest($accessToken)
);

// Get our spreadsheet
$spreadsheet = (new Google\Spreadsheet\SpreadsheetService)
    ->getSpreadsheetFeed()
    ->getByTitle('ITP');

// Get the first worksheet (tab)
$worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
$worksheet = $worksheets[1];


$listFeed = $worksheet->getListFeed();

$name = $_POST['name'];
$tel = $_POST['tel'];
$name =  htmlspecialchars($name, ENT_QUOTES);
$tel = htmlspecialchars($tel, ENT_QUOTES);
$date = date('d.m.Y');

$listFeed->insert([
    'name' => $name,
    'phone' => $tel,
    'date' => $date,
]);

$hire_us_message = 'Имя: ' . $name . "\r\n";
$hire_us_message .= 'Телефон: ' . $tel . "\r\n";
$file = fopen('consultation.csv', 'a');
$request = array($name, $email);
fputcsv($file, $request);
fclose($file);
$to = 'as.antonuk@gmail.com, info@sempal.com.ua';
$subject = 'Лендинг ИТП: запрос "ПРОКОНСУЛЬТИРОВАТЬСЯ"';
$headers = 'From: info@sempal.com.ua' . "\r\n" . 'Reply-To: ' . $email;
mail($to, $subject, $hire_us_message, $headers);
