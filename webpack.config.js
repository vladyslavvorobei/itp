'use strict';
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './src/app.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './js/app.js',
    },
    module: {
       
        rules: [ 
        //style
            {
                test: /\.s?css$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    use: [{
                        loader: 'css-loader'
                    }, {
                        loader: 'postcss-loader',
                    options: {
                        plugins: (loader) => [
                        require('postcss-smart-import'),
                        require('autoprefixer'),
                        ]
                    }
                }, {
                    loader: 'sass-loader'
                }]
            })},
            //template
            {
                test: /\.hbs$/,
                use: [{
                    loader: "handlebars-loader",
                    query: {
                        partialDirs: [__dirname + '/src/hbs']
                    }
                }]
            }, 
            //js 
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            },
            //img 
            {
                test: /\.(jpg|jpeg|png)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'assets/img/',
                        publicPath: 'assets/img/'
                    }
            
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new HtmlWebpackPlugin({
            filename: './index.html',
            template: './src/hbs/template.hbs'
        }),
        new ExtractTextPlugin({
            filename: "/css/styles.css",
            disable: false,
            allChunks: true
        }),
        new CopyWebpackPlugin([
        {
            from: './src/assets/fonts',
            to: './assets/fonts'
        },
        {
            from: './src/php/code',
            to: './php/code'
        },
        {
            from: './src/assets/img',
            to: './assets/img'
        }]),
        new BrowserSyncPlugin({
            port: 3000,
            server: { baseDir: ['dist'] },
            reload: false,
            open: true
        })
    ]
}